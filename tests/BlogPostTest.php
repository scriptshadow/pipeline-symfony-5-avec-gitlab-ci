<?php

namespace App\Tests;

use App\Entity\Post;
use PHPUnit\Framework\TestCase;

class BlogPostTest extends TestCase
{
    public function testPost(): void
    {

        $post = new Post();
        $post->setName("Bonjour tout le monde");

        $this->assertTrue($post->getName() === "Bonjour tout le monde");
    }
}
