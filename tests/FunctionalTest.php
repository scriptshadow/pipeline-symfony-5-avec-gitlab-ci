<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest extends WebTestCase
{
    public function testShouldDisplayPost(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/post');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Post index');
    }

    public function testShouldDisplayCreateNewPost(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/post/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Post');
    }
    public function testShouldDisplayAddNewPost(): void
    {
        $client = static::createClient();
        $client->followRedirects();
        $crawler = $client->request('GET', '/post/new');

        $buttonCrawlerNode = $crawler->selectButton('Save');

        $form = $buttonCrawlerNode->form();
        $uuid = uniqid();

        $form = $buttonCrawlerNode->form(["post[name]" => "Add post for test" . $uuid]);
        $client->submit($form);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('body', "Add post for test" . $uuid);
    }
}
